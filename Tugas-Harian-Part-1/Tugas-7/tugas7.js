let dataPeserta = ["john", "laki-laki", "programmer", "30"]

let output = `Halo, nama saya ${dataPeserta[0]}. saya ${dataPeserta[1]} berumur ${dataPeserta[3]} bekerja sebagai seorang ${dataPeserta[2]}`;

console.log(output)

let array1 = ["selamat", "anda", "melakukan", "perulangan", "array", "dengan", "for"];

for (let i = 0; i < array1.length; i++){
    console.log(array1[i]);
}

let array2 = [1, 2, 3, 4, 5, 6,7, 8, 9, 10];
let hasil;
for (let i = 0; i < array2.length; i++){
    hasil = array2[i] % 2;
    if (hasil === 0) {
        console.log(array2[i]);
    }
}

var sayuran=[];
 sayuran.push("Kangkung");
 sayuran.push("Bayam");
 sayuran.push("Buncis");
 sayuran.push("Kubis");
 sayuran.push("Timun");
 sayuran.push("Seledri");
 sayuran.push("Tauge");
 console.log(sayuran);